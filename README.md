# RBC Backend Assignment


This is the RBC backend assignment created by Liangbicheng (Ralph) Wang.

This springBoot Application comes with three different APIs:

	1) /upload - upload a bulk data set

	2) /query/${stock} - query for data by stock ticker

	3) /add - add a new stock record

Default springBoot configuration has been used: port 8080 etc.

JavaSE-1.8 has been used for the development.

To start the application, simply go to target folder and run
	'java -jar backend.assignment-0.0.1-SNAPSHOT.jar'