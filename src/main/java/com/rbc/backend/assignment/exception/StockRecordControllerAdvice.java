package com.rbc.backend.assignment.exception;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.rbc.backend.assignment.controller.StockRecordController;

@RestControllerAdvice(assignableTypes = StockRecordController.class)
public class StockRecordControllerAdvice {

	@ExceptionHandler(Exception.class)
	public final ResponseEntity handleAllExceptions(Exception e) {
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
	}

	@ExceptionHandler(IOException.class)
	public final ResponseEntity handleIOExceptions(IOException e) {
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
	}

	@ExceptionHandler(FileNotFoundException.class)
	public final ResponseEntity handleFileNotFoundExceptions(FileNotFoundException e) {
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
	}
}
