package com.rbc.backend.assignment.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.rbc.backend.assignment.dto.StockRecordDto;

public interface StockRecordService {

	public boolean upload(MultipartFile file) throws Exception;

	public List<StockRecordDto> queryByStock(String stock) throws Exception;

	public boolean addStockRecord(StockRecordDto stockRecordDto) throws Exception;
}
