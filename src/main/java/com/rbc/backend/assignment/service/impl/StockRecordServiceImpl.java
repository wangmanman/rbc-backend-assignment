package com.rbc.backend.assignment.service.impl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.opencsv.CSVReader;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.HeaderColumnNameTranslateMappingStrategy;
import com.rbc.backend.assignment.constants.StockRecordConstants;
import com.rbc.backend.assignment.dto.StockRecordDto;
import com.rbc.backend.assignment.model.StockRecord;
import com.rbc.backend.assignment.service.StockRecordService;

@Component
public class StockRecordServiceImpl implements StockRecordService {

	HeaderColumnNameTranslateMappingStrategy strategy;
	
	File flatFile;

    private ModelMapper modelMapper;

	StockRecordServiceImpl() {
		strategy = new HeaderColumnNameTranslateMappingStrategy();
		strategy.setType(StockRecord.class);
		strategy.setColumnMapping(StockRecordConstants.csvFieldMap);

		modelMapper = new ModelMapper();
		
		flatFile = new File(StockRecordConstants.stockRecordFlatFile);
	}

	@Override
	public boolean upload(MultipartFile file) throws IOException {
		saveFile(file);
		if (flatFile.exists()) {
			return true;
		}
		else return false;

	}

	private List<StockRecord> parseFile(File file) throws FileNotFoundException {
		CSVReader csvReader = new CSVReader(new FileReader(file.getAbsolutePath()));
		CsvToBean csv = new CsvToBean();
		csv.setMappingStrategy(strategy);
		csv.setCsvReader(csvReader);
		System.out.println("FlatFile data parsed!");
		return csv.parse();
	}

	private boolean saveFile(MultipartFile file) throws IOException {
		byte[] bytes = file.getBytes();
		if (flatFile.createNewFile()) {
			System.out.println("FlatFile does not exist, new file created!");
		} else {
			System.out.println("FlatFile already exist, old data will be overwritten!");
		}
		Files.write(flatFile.toPath(), bytes);
		return true;
	}

	@Override
	public boolean addStockRecord(StockRecordDto stockRecordDto) throws Exception {

		StockRecord newStockRecord = new StockRecord();

		newStockRecord.setQuarter(stockRecordDto.getQuarter());
		newStockRecord.setStock(stockRecordDto.getStock());
		newStockRecord.setDate(stockRecordDto.getDate());
		newStockRecord.setOpen(stockRecordDto.getOpen());
		newStockRecord.setHigh(stockRecordDto.getHigh());
		newStockRecord.setLow(stockRecordDto.getLow());
		newStockRecord.setClose(stockRecordDto.getClose());
		newStockRecord.setVolume(stockRecordDto.getVolume());
		newStockRecord.setPercentChangePrice(stockRecordDto.getPercentChangePrice());
		newStockRecord.setPercentChangeVolumeOverLastWeek(stockRecordDto.getPercentChangeVolumeOverLastWeek());
		newStockRecord.setPreviousWeeksVolume(stockRecordDto.getPreviousWeeksVolume());
		newStockRecord.setNextWeeksOpen(stockRecordDto.getNextWeeksOpen());
		newStockRecord.setNextWeeksClose(stockRecordDto.getNextWeeksClose());
		newStockRecord.setPercentChangeNextWeeksPrice(stockRecordDto.getPercentChangeNextWeeksPrice());
		newStockRecord.setDaysToNextDividend(stockRecordDto.getDaysToNextDividend());
		newStockRecord.setPercentReturnNextDividend(stockRecordDto.getPercentReturnNextDividend());

		String stockRecordInCSV = newStockRecord.getDataInCSV();

		FileWriter fileWriter;
		BufferedWriter bufferedWriter;

		try {
			fileWriter = new FileWriter(flatFile, true);
			bufferedWriter = new BufferedWriter(fileWriter);
			bufferedWriter.write(stockRecordInCSV);
			bufferedWriter.close();
			fileWriter.close();
			System.out.println("New stock record has been added!");
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public List<StockRecordDto> queryByStock(String stock) throws FileNotFoundException {
        List<StockRecord> stockRecordList = parseFile(flatFile);
        List<StockRecordDto> result = new ArrayList<StockRecordDto>();

        if (stockRecordList != null) {
        	for (StockRecord stockRecord: stockRecordList) {
        		if (stockRecord.getStock().equals(stock)) {
        			System.out.println("Found record for stock " + stock + ", adding into result!");
        			result.add(modelMapper.map(stockRecord, StockRecordDto.class));
        		}
        	}
        }
        return result;
    }
}
