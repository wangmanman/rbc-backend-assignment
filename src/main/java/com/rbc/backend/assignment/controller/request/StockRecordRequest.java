package com.rbc.backend.assignment.controller.request;

import java.math.BigDecimal;
import java.math.BigInteger;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
// Validation logic might be applied to each of the field in this request
public class StockRecordRequest {

	private Integer quarter;

	private String stock;

	private String date;

	private String open;

	private String high;

	private String low;

	private String close;

	private BigInteger volume;

	@JsonProperty("percent_change_price")
	private BigDecimal percentChangePrice;

	@JsonProperty("percent_change_volume_over_last_wk")
	private BigDecimal percentChangeVolumeOverLastWeek;

	@JsonProperty("previous_weeks_volume")
	private BigInteger previousWeeksVolume;

	@JsonProperty("next_weeks_open")
	private String nextWeeksOpen;

	@JsonProperty("next_weeks_close")
	private String nextWeeksClose;

	@JsonProperty("percent_change_next_weeks_price")
	private BigDecimal percentChangeNextWeeksPrice;

	@JsonProperty("days_to_next_dividend")
	private Integer daysToNextDividend;

	@JsonProperty("percent_return_next_dividend")
	private BigDecimal percentReturnNextDividend;
}
