package com.rbc.backend.assignment.controller.response;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class StockRecordResponse {

	LocalDateTime timestamp;
	
	String message;

	private StockRecordResponse() {
		timestamp = LocalDateTime.now();
		message = "N/A";
	}

	public StockRecordResponse(String message) {
		this();
		this.message = message;
	}
}
