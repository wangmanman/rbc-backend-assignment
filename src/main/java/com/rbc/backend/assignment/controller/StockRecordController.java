package com.rbc.backend.assignment.controller;

import java.io.IOException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.rbc.backend.assignment.controller.request.StockRecordRequest;
import com.rbc.backend.assignment.controller.response.StockRecordResponse;
import com.rbc.backend.assignment.dto.StockRecordDto;
import com.rbc.backend.assignment.service.StockRecordService;

@RestController
public class StockRecordController {

	@Autowired
	private StockRecordService stockService;

	@PostMapping("/upload")
	public ResponseEntity<StockRecordResponse> upload(@RequestParam("file") MultipartFile file) throws Exception {
		if (stockService.upload(file)) {
			return ResponseEntity.ok(new StockRecordResponse("Bulk data set has been successfully uploaded!"));
		}
		else return ResponseEntity.ok(new StockRecordResponse("Failed to upload bulk data set, please try again later..."));
	}

	@GetMapping("/query/{stock}")
	public ResponseEntity<List<StockRecordDto>> query(@PathVariable("stock") String stock) throws Exception {
		return ResponseEntity.ok(stockService.queryByStock(stock));
	}

	@RequestMapping(value = "/add", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.POST)
	public ResponseEntity<StockRecordResponse> add(@RequestBody @Valid StockRecordRequest addStockRequest) throws Exception {
		StockRecordDto newStockRecord = new StockRecordDto();

		newStockRecord.setQuarter(addStockRequest.getQuarter());
		newStockRecord.setStock(addStockRequest.getStock());
		newStockRecord.setDate(addStockRequest.getDate());
		newStockRecord.setOpen(addStockRequest.getOpen());
		newStockRecord.setHigh(addStockRequest.getHigh());
		newStockRecord.setLow(addStockRequest.getLow());
		newStockRecord.setClose(addStockRequest.getClose());
		newStockRecord.setVolume(addStockRequest.getVolume());
		newStockRecord.setPercentChangePrice(addStockRequest.getPercentChangePrice());
		newStockRecord.setPercentChangeVolumeOverLastWeek(addStockRequest.getPercentChangeVolumeOverLastWeek());
		newStockRecord.setPreviousWeeksVolume(addStockRequest.getPreviousWeeksVolume());
		newStockRecord.setNextWeeksOpen(addStockRequest.getNextWeeksOpen());
		newStockRecord.setNextWeeksClose(addStockRequest.getNextWeeksClose());
		newStockRecord.setPercentChangeNextWeeksPrice(addStockRequest.getPercentChangeNextWeeksPrice());
		newStockRecord.setDaysToNextDividend(addStockRequest.getDaysToNextDividend());
		newStockRecord.setPercentReturnNextDividend(addStockRequest.getPercentReturnNextDividend());

		if (stockService.addStockRecord(newStockRecord)) {
			return ResponseEntity.ok(new StockRecordResponse("Stock record set has been successfully uploaded!"));
		}
		else return ResponseEntity.ok(new StockRecordResponse("Failed to add stock record, please try again later..."));
	}
}
