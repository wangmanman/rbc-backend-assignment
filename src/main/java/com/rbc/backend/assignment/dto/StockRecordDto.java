package com.rbc.backend.assignment.dto;

import java.math.BigDecimal;
import java.math.BigInteger;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class StockRecordDto {
	private Integer quarter;

	private String stock;

	private String date;

	private String open;

	private String high;

	private String low;

	private String close;

	private BigInteger volume;

	private BigDecimal percentChangePrice;

	private BigDecimal percentChangeVolumeOverLastWeek;

	private BigInteger previousWeeksVolume;

	private String nextWeeksOpen;

	private String nextWeeksClose;

	private BigDecimal percentChangeNextWeeksPrice;

	private Integer daysToNextDividend;

	private BigDecimal percentReturnNextDividend;
}
