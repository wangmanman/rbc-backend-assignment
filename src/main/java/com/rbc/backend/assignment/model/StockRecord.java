package com.rbc.backend.assignment.model;

import java.math.BigDecimal;
import java.math.BigInteger;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class StockRecord {

	//private static final SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy"); 
	// As possible values for quarter are in {1,2,3,4}, data type Integer is used
	private Integer quarter;

	// Stock name will be Alphanumeric from my point of view, so String is used
	private String stock;

	private String date;

	private String open;

	private String high;

	private String low;

	private String close;

	private BigInteger volume;

	private BigDecimal percentChangePrice;

	private BigDecimal percentChangeVolumeOverLastWeek;

	private BigInteger previousWeeksVolume;

	private String nextWeeksOpen;

	private String nextWeeksClose;

	private BigDecimal percentChangeNextWeeksPrice;

	private Integer daysToNextDividend;

	private BigDecimal percentReturnNextDividend;

	/*public void setDate(String dateStr) {  
		try {
			date = formatter.parse(dateStr);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/

	public String getDataInCSV() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(quarter + ",")
			.append(stock + ",")
			.append(date + ",")
			.append(open + ",")
			.append(high + ",")
			.append(low + ",")
			.append(volume + ",")
			.append(percentChangePrice + ",")
			.append(percentChangeVolumeOverLastWeek + ",")
			.append(previousWeeksVolume + ",")
			.append(nextWeeksOpen + ",")
			.append(nextWeeksClose + ",")
			.append(percentChangeNextWeeksPrice + ",")
			.append(daysToNextDividend + ",")
			.append(percentReturnNextDividend + "\n");
		return stringBuilder.toString();
	}
}
