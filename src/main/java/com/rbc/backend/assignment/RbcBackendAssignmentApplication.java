package com.rbc.backend.assignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RbcBackendAssignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(RbcBackendAssignmentApplication.class, args);
	}
}
