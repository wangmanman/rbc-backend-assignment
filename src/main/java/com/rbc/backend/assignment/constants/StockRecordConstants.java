package com.rbc.backend.assignment.constants;

import java.util.Map;

public class StockRecordConstants {

	public static final Map<String, String> csvFieldMap = Map.ofEntries(
	        Map.entry("quarter", "quarter"),
	        Map.entry("stock", "stock"),
	        Map.entry("date", "date"),
	        Map.entry("open", "open"),
	        Map.entry("high", "high"),
	        Map.entry("low", "low"),
	        Map.entry("close", "close"),
	        Map.entry("volume", "volume"),
	        Map.entry("percent_change_price", "percentChangePrice"),
	        Map.entry("percent_change_volume_over_last_wk", "percentChangeVolumeOverLastWeek"),
	        Map.entry("previous_weeks_volume", "previousWeeksVolume"),
	        Map.entry("next_weeks_open", "nextWeeksOpen"),
	        Map.entry("next_weeks_close", "nextWeeksClose"),
	        Map.entry("percent_change_next_weeks_price", "percentChangeNextWeeksPrice"),
	        Map.entry("days_to_next_dividend", "daysToNextDividend"),
	        Map.entry("percent_return_next_dividend", "percentReturnNextDividend")
	);

	public static final String stockRecordFlatFile = "dow_jones_index.data";
}
